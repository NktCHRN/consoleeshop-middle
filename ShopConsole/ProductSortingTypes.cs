﻿namespace ShopConsole
{
    public enum ProductSortingTypes
    {
        PriceAscending,
        PriceDescending
    }
}
