﻿using ShopLogic.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;
using ShopLogic;

namespace ShopConsole
{
    public delegate void BuyThing(IProduct product);

    public static class GuestMode
    {
        public static IShop Shop { get; set; }

        public static BuyThing BuyThingDelegate { get; set; }

        private static readonly Dictionary<short, Action> _guestMenuActions = new Dictionary<short, Action>()
        {
            {1, PrintShowCase },
            {2, CustomerMode.Register },
            {3, Login }
        };

        public static void PrintMenu()
        {
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            PrintHelp();
            bool parsed;
            short choice;
            const short minPoint = 0;
            short maxPoint = (short)(_guestMenuActions.Keys.Max() + 1);
            do
            {
                BuyThingDelegate = PrintProductInfoUnbuyable;
                Console.WriteLine($"\nEnter the number {minPoint} - {maxPoint}: ");
                parsed = short.TryParse(Console.ReadLine(), out choice);
                if (!parsed || choice < minPoint || choice > maxPoint)
                {
                    Console.WriteLine($"Error: you entered not a number or number was smaller than {minPoint} or bigger than {maxPoint}.");
                    Console.WriteLine($"Quit - {minPoint}");
                    choice = minPoint - 1;
                }
                try
                {
                    _guestMenuActions[choice]?.Invoke();
                }
                catch (KeyNotFoundException) { }
                if (choice > minPoint && choice <= maxPoint)
                {
                    Program.PrintHeader();
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    PrintHelp();
                }
            } while (choice != minPoint);
        }

        public static void PrintShowCase()
        {
            const short halfCategories = 5;
            const short totallyCategories = 10;
            const short sortsQuantity = 2;
            const short numberLength = -4;
            const short nameMaxLength = -20;
            const short categoryMaxLength = -20;
            const short priceMaxLength = -10;
            string find = string.Empty;
            bool findMode = false;
            short filter = -1;
            short sortBy = 0;
            string entered;
            short temp;
            IGettableProductCollection productBase;
            List<(IProduct, int)> products;
            do
            {
                productBase = Shop.ShowCase;
                products = GetSorted(GetOnSale(productBase), (ProductSortingTypes)sortBy).ToList();
                if (filter > -1)
                    products = GetFiltered(products, (Categories)filter).ToList();
                if (findMode)
                    products = Find(products, find).ToList();
                Program.PrintHeader();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Products available:");
                Console.WriteLine($"Filter: f{filter + 1}; Sort by s{sortBy + 1}; FindMode: {(findMode ? "On" : "Off")}");
                Console.Write("[Filters: f0 - All; ");
                for (int i = 1; i <= halfCategories; i++)
                    Console.Write($"f{i} - {(Categories)(i - 1)}; ");
                Console.WriteLine();
                for (int i = halfCategories; i <= totallyCategories; i++)
                    Console.Write($"f{i} - {(Categories)(i - 1)}; ");
                Console.WriteLine("]");

                Console.Write("[Sorting types: ");
                for (int i = 1; i <= sortsQuantity; i++)
                    Console.Write($"s{i} - {(ProductSortingTypes)(i - 1)}; ");
                Console.WriteLine("]");
                Console.WriteLine("[To find a product, enter: find name]");
                Console.WriteLine("[To stop searching and get back to full menu: stopfind]");
                Console.WriteLine("[To view precise description (and then buy) any item, just enter it's number (ex., 1)]");
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("[0 - quit]");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine($"\n{"№", numberLength}{"Name", nameMaxLength}{"Category", categoryMaxLength}{"Price", priceMaxLength}");
                for (int i = 0; i < products.Count; i++)
                    Console.WriteLine($"{i + 1, numberLength}{products[i].Item1.Name, nameMaxLength}{products[i].Item1.Category, categoryMaxLength}{string.Format("{0:F2}", products[i].Item1.Price) + " UAH", priceMaxLength}");
                Console.WriteLine("\nEnter any filter, sorting type or number of product you want to view description of or buy. 0 - quit");
                entered = Console.ReadLine().Trim().ToLower();
                if (entered.Length > 0)
                {
                    if (char.IsLetter(entered[0]) && entered.Length > 1)
                    {
                        if (entered[0] == 'f')
                        {
                            if (char.IsDigit(entered[1]))
                            {
                                short.TryParse(entered.Remove(0, 1), out temp);
                                if (temp >= 0 && temp <= totallyCategories)
                                {
                                    temp--;
                                    filter = temp;
                                }
                            }
                            else if (entered.StartsWith("find "))
                            {
                                find = entered.Remove(0, 5);
                                findMode = true;
                            }
                        }
                        else if (entered[0] == 's')
                        {
                            if (char.IsDigit(entered[1]))
                            {
                                short.TryParse(entered.Remove(0, 1), out temp);
                                if (temp > 0 && temp <= sortsQuantity)
                                {
                                    temp--;
                                    sortBy = temp;
                                }
                            }
                            else if (entered.StartsWith("stopfind"))
                            {
                                findMode = false;
                            }
                        }
                    }
                    else
                    {
                        int.TryParse(entered, out int enteredProductNumber);
                        enteredProductNumber--;
                        if (enteredProductNumber >= 0 && enteredProductNumber < products.Count)
                            BuyThingDelegate?.Invoke(products[enteredProductNumber].Item1);
                    }
                }
            } while (entered != "0");
            Console.ResetColor();
        }

        public static IEnumerable<(IProduct, int)> Find(IEnumerable<(IProduct, int)> products, string name)
        {
            if (products == null)
                throw new ArgumentNullException(nameof(products), "Products cannot be null");
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Name of the product cannot be null");
            name = name.ToLower();
            return from item in products
                    where item.Item1.Name.ToLower().Contains(name)
                    select item;
        }

        public static IEnumerable<(IProduct, int)> GetSorted(IEnumerable<(IProduct, int)> products, ProductSortingTypes sortBy)
        {
            if (products == null)
                throw new ArgumentNullException(nameof(products), "Products cannot be null");
            if (sortBy == ProductSortingTypes.PriceAscending)
            {
                return from item in products
                       orderby item.Item1.Price, item.Item1.Category, item.Item1.Name
                       select item;
            }
            else
            {
                return from item in products
                       orderby item.Item1.Price descending, item.Item1.Category, item.Item1.Name
                       select item;
            }
        }

        public static IEnumerable<(IProduct, int)> GetOnSale(IEnumerable<(IProduct, int)> products)
        {
            if (products == null)
                throw new ArgumentNullException(nameof(products), "Products cannot be null");
            return from item in products
                    where item.Item2 > 0
                    select item;
        }

        public static IEnumerable<(IProduct, int)> GetFiltered(IEnumerable<(IProduct, int)> products, Categories filter)
        {
            if (products == null)
                throw new ArgumentNullException(nameof(products), "Products cannot be null");
            return from item in products
                    where item.Item1.Category == filter
                    select item;
        }

        public static void PrintProductInfoUnbuyable(IProduct product)
        {
            if (product == null)
                throw new ArgumentNullException(nameof(product), "Product cannot be null");
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Product info");
            Console.WriteLine($"Name: {product.Name}");
            Console.WriteLine($"Category: {product.Category}");
            Console.WriteLine($"Description: {product.Description}");
            Console.WriteLine($"Price: {string.Format("{0:F2}", product.Price)} UAH");
            Console.WriteLine("\nTo buy this thing, log in or register as customer");
            Console.WriteLine("\nPress [ENTER] to go back to showcase");
            Console.ReadLine();
        }

        public static void Login()
        {
            IAuthorizable user;
            string login;
            bool firstTime = true;
            bool quit;
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            do
            {
                quit = false;
                Console.WriteLine("Enter your login (phone number of email):");
                login = Console.ReadLine();
                if (Shop.Admin.CheckLogin(login))
                {
                    user = Shop.Admin;
                    if (CheckPassword(user))
                        AdminMode.PrintMenu(user as IAdmin);
                    quit = true;
                }
                else
                {
                    foreach (IAuthorizable customer in Shop.Customers)
                    {
                        if (customer.CheckLogin(login))
                        {
                            user = customer;
                            if (CheckPassword(user))
                                CustomerMode.PrintMenu(user as ICustomer);
                            quit = true;
                            break;
                        }
                    }
                    if (!quit && !firstTime && (login == "0" || login == "1"))
                    {
                        if (login == "1")
                            CustomerMode.Register();
                        quit = true;
                    }
                    else if (!quit)
                    {
                        Console.WriteLine("You entered the wrong login");
                        Console.WriteLine("Press 0 to quit or 1 to register a new account");
                    }
                }
                firstTime = false;
            } while (!quit);
        }

        public static bool CheckPassword(IAuthorizable user)
        {
            if (user == null)
                throw new ArgumentNullException(nameof(user), "User cannot be null");
            string password;
            bool firstTime = true;
            Console.WriteLine("\nEnter your password:");
            password = Program.GetHiddenConsoleInput();
            while (!user.CheckPassword(password) && (password != "0" || firstTime))
            {
                Console.WriteLine("You have entered the wrong password");
                Console.WriteLine("Enter your password or 0 to quit");
                password = Program.GetHiddenConsoleInput();
                if (firstTime)
                    firstTime = false;
            }
            return password != "0";
        }

        public static void PrintHelp()
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Menu: ");
            Console.WriteLine("1. View goods");
            Console.WriteLine("2. Register");
            Console.WriteLine("3. Log in");
            Console.WriteLine("4. Help");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("0. Quit");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
        }
    }
}
