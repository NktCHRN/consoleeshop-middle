﻿using ShopLogic.Interfaces;
using ShopLogic.HelperClasses;
using System;
using System.Collections.Generic;
using ShopLogic;
using System.Linq;

namespace ShopConsole
{
    public static class CustomerMode
    {
        public static IBuyer Buyer { get; set; }

        private static INotifiable _customerNotifiable;

        public static IPerson Person { get; set; }

        public static IAuthorizable PersonAuthorizable { get; set; }

        public static IOrder Cart { get; set; }

        private static readonly Dictionary<short, Action> _customerMenuActions = new Dictionary<short, Action>()
        {
            {1, PrintShowCase},
            {2, PrintCart },
            {3, PrintOrders },
            {4, PrintSentOrders },
            {5, PrintNotReceivedOrders },
            {6, () => PrintInfo(false, true) }
        };

        public static void Register()
        {
            ICustomer newCustomer;
            string firstName, lastName, email, phoneNumber, password;
            DateTime birthday;
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            firstName = EnterFirstName();
            if (firstName == "0")
                return;
            lastName = EnterLastName();
            if (lastName == "0")
                return;
            birthday = EnterBirthday();
            if (birthday == DateTime.MinValue)
                return;
            email = EnterEmail();
            if (email == "0")
                return;
            phoneNumber = EnterPhoneNumber();
            if (phoneNumber == "0")
                return;
            password = EnterPassword();
            if (password == "0")
                return;
            newCustomer = new Customer(firstName, lastName, birthday, email, phoneNumber, password, GuestMode.Shop.Admin);
                GuestMode.Shop.Customers.Add(newCustomer);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\nCongratulations!");
                Console.WriteLine("Your sucessfully registered your account");
                Console.WriteLine("You can use your email or phone number as a login");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("\nPress [ENTER] to continue");
                Console.ReadLine();
                PrintMenu(newCustomer);
        }

        public static string EnterFirstName()
        {
            bool firstTime = true;
            Console.WriteLine("\nEnter the first name:");
            string firstName = Console.ReadLine();
            while (!firstName.IsValidName() && (firstName != "0" || firstTime))
            {
                Console.WriteLine("You entered not a name");
                Console.WriteLine("Enter the first name once more or 0 to cancel the operation:");
                firstName = Console.ReadLine();
                if (firstTime)
                    firstTime = false;
            }
            return firstName;
        }

        public static string EnterLastName()
        {
            bool firstTime = true;
            Console.WriteLine("\nEnter the last name:");
            string lastName = Console.ReadLine();
            while (!lastName.IsValidName() && (lastName != "0" || firstTime))
            {
                Console.WriteLine("You entered not a name");
                Console.WriteLine("Enter the last name once more or 0 to cancel the operation:");
                lastName = Console.ReadLine();
                if (firstTime)
                    firstTime = false;
            }
            return lastName;
        }

        public static DateTime EnterBirthday()
        {
            bool firstTime = true;
            DateTime birthday;
            string birthdayString;
            Console.WriteLine("\nEnter the date of birth: ");
            birthdayString = Console.ReadLine();
            while ((!DateTime.TryParse(birthdayString, out birthday) || birthday.Year < 1870 || birthday > DateTime.Now) && (birthdayString != "0" || firstTime))
            {
                Console.WriteLine("You have entered the wrong date");
                Console.WriteLine("Please, divide the date with slashes / or dots . ");
                Console.WriteLine("Enter the date of birth once more or 0 to cancel the operation: ");
                birthdayString = Console.ReadLine();
                if (firstTime)
                    firstTime = false;
            }
            if (birthdayString == "0")
                birthday = DateTime.MinValue;
            return birthday;
        }

        public static string EnterEmail()
        {
            bool firstTime = true;
            bool rerun;
            string email;
            Console.WriteLine("\nEnter the email:");
            do
            {
                rerun = false;
                email = Console.ReadLine();
                if (email == "0" && !firstTime)
                {
                    rerun = false;
                }
                else if (!email.IsValidEmail())
                {
                    Console.WriteLine("You have entered invalid email");
                    Console.WriteLine("Enter the email once more or 0 to cancel the operation:");
                    rerun = true;
                }
                else if (!GuestMode.Shop.IsFreeLogin(email))
                {
                    Console.WriteLine("You email is already registered");
                    Console.WriteLine("Enter the email once more or 0 to cancel the operation:");
                    rerun = true;
                }
                if (firstTime)
                    firstTime = false;
            } while (rerun);
            return email;
        }

        public static string EnterPhoneNumber()
        {
            bool firstTime = true;
            bool rerun;
            string phoneNumber;
            Console.WriteLine("\nEnter the phone number:");
            do
            {
                rerun = false;
                phoneNumber = Console.ReadLine();
                if (phoneNumber == "0" && !firstTime)
                {
                    rerun = false;
                }
                else if (!phoneNumber.IsValidPhoneNumber())
                {
                    Console.WriteLine("You have entered invalid phone number");
                    Console.WriteLine("Enter the phone number once more or 0 to cancel the operation:");
                    rerun = true;
                }
                else if (!GuestMode.Shop.IsFreeLogin(phoneNumber))
                {
                    Console.WriteLine("You phone number is already registered");
                    Console.WriteLine("Enter the phone number once more or 0 to cancel the operation:");
                    rerun = true;
                }
                if (firstTime)
                    firstTime = false;
            } while (rerun);
            return phoneNumber;
        }

        public static string EnterPassword()
        {
            bool firstTime = true;
            Console.WriteLine("\nEnter the new password: ");
            string password = Program.GetHiddenConsoleInput();
            while (!password.IsValidPassword() && (password != "0" || firstTime))
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Unfortunately, you entered not a valid password");
                Console.WriteLine("Password should: ");
                Console.WriteLine("- have the length of at least 8 characters");
                Console.WriteLine("- have at least one digit");
                Console.WriteLine("- have at least uppercase letter");
                Console.WriteLine("- have at least lowercase letter");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Please, enter the password once more or 0 to cancel the operation: ");
                password = Program.GetHiddenConsoleInput();
                if (firstTime)
                    firstTime = false;
            };
            return password;
        }

        public static void PrintNotifications()
        {
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"Hi, {Person.GetFullName()}!");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            var notifications = _customerNotifiable.UnreadNotifications;
            Console.WriteLine($"\nYou have {notifications.Count} unread notifications:");
            for (int i = 0; i < notifications.Count; i++)
                Console.WriteLine($"{i + 1}. The status of your order №{notifications[i].eventArgs.Order.ID} has been changed from {notifications[i].eventArgs.LastStatus}" +
                    $" to {notifications[i].eventArgs.NewStatus} by {notifications[i].sender}");
            _customerNotifiable.ClearNotifications();
            Console.WriteLine("\nPress [ENTER] to continue");
            Console.ReadLine();
        }


        public static void PrintMenu(ICustomer customer)
        {
            Buyer = customer ?? throw new ArgumentNullException(nameof(customer), "Customer cannot be null");
            _customerNotifiable = customer;
            Person = customer;
            PersonAuthorizable = customer;
            Cart = new Order(Buyer);
            if (_customerNotifiable.UnreadNotifications.Count > 0)
                PrintNotifications();
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine($"Hi, {Person.GetFullName()}!");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            PrintHelp();
            bool parsed;
            short choice;
            const short minPoint = 0;
            short maxPoint = (short)(_customerMenuActions.Keys.Max() + 1);
            do
            {
                Console.WriteLine($"\nEnter the number {minPoint} - {maxPoint}: ");
                parsed = short.TryParse(Console.ReadLine(), out choice);
                if (!parsed || choice < minPoint || choice > maxPoint)
                {
                    Console.WriteLine($"Error: you entered not a number or number was smaller than {minPoint} or bigger than {maxPoint}.");
                    Console.WriteLine($"Quit - {minPoint}");
                    choice = minPoint - 1;
                }
                try
                {
                    _customerMenuActions[choice]?.Invoke();
                }
                catch (KeyNotFoundException) { }
                if (choice > minPoint && choice <= maxPoint)
                {
                    Program.PrintHeader();
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine($"Hi, {Person.GetFullName()}!");
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    PrintHelp();
                }
            } while (choice != minPoint);
            ClearCart();
        }

        public static void ClearCart()
        {
            foreach (var item in Cart)
                GuestMode.Shop.ShowCase.Add(item);
            Cart.Clear();
        }

        public static void PrintProductInfoBuyable(IProduct product)
        {
            if (product == null)
                throw new ArgumentNullException(nameof(product), "Product cannot be null");
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Product info");
            Console.WriteLine($"Name: {product.Name}");
            Console.WriteLine($"Category: {product.Category}");
            Console.WriteLine($"Description: {product.Description}");
            Console.WriteLine($"Price: {string.Format("{0:F2}", product.Price)} UAH");
            Console.WriteLine();
            string entered, temp;
            bool parsed;
            do
            {
                Console.WriteLine("To add this thing to cart, enter \"buy quantity\" (for example, buy 2))");
                entered = Console.ReadLine().Trim();
                if (entered.StartsWith("buy "))
                {
                    temp = entered.Remove(0, 4);
                    parsed = int.TryParse(temp, out int quantity);
                    if (!parsed || quantity <= 0)
                    {
                        Console.WriteLine("You entered the wrong quantity (negative or equal 0)");
                    }
                    else
                    {
                        try
                        {
                            GuestMode.Shop.ShowCase.Sell(GuestMode.Shop.ShowCase.Find(product), quantity);
                            Cart.Add((product, quantity));
                            Console.ForegroundColor = ConsoleColor.Green;
                            Console.WriteLine("\nCongratulations!");
                            Console.WriteLine($"You successfully added the product to cart");
                            Console.ForegroundColor = ConsoleColor.DarkGreen;
                            Console.WriteLine("\nPress [ENTER] to go back to menu");
                            Console.ReadLine();
                            entered = "0";
                        }
                        catch (ArgumentException)
                        {
                            Console.WriteLine("Unfortunately, the shop does not have such a quantity of this product.");
                            Console.WriteLine("Please, enter less quantity");
                        }
                    }
                }
            } while (entered != "0");
        }

        public static void PrintShowCase()
        {
            GuestMode.BuyThingDelegate = PrintProductInfoBuyable;
            GuestMode.PrintShowCase();
        }

        public static void ChangeFirstName()
        {
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            string firstName = EnterFirstName();
            if (firstName != "0")
            {
                Person.FirstName = firstName;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\nCongratulations!");
                Console.WriteLine($"You successfully changed the first name to {Person.FirstName}");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("\nPress [ENTER] to continue");
                Console.ReadLine();
            }
        }

        public static void ChangeLastName()
        {
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            string lastName = EnterLastName();
            if (lastName != "0")
            {
                Person.LastName = lastName;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\nCongratulations!");
                Console.WriteLine($"You successfully changed the last name to {Person.LastName}");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("\nPress [ENTER] to continue");
                Console.ReadLine();
            }
        }

        public static void ChangeBirthday()
        {
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            DateTime birthday = EnterBirthday();
            if (birthday != DateTime.MinValue)
            {
                Person.Birthday = birthday;
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("\nCongratulations!");
                Console.WriteLine($"You successfully changed the birthday to {Person.Birthday.ToShortDateString()}");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("\nPress [ENTER] to continue");
                Console.ReadLine();
            }
        }

        public static void ChangeEmail(bool confirmation)
        {
            bool confirmed;
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            if (confirmation)
            {
                Console.WriteLine("Before changing data that can be used as a login, you need to confirm your identity first");
                confirmed = GuestMode.CheckPassword(PersonAuthorizable);
            }
            else
            {
                confirmed = true;
            }
            if (confirmed)
            {
                Console.WriteLine("Now, you can change the email");
                string email = EnterEmail();
                if (email != "0")
                {
                    Person.Email = email;
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("\nCongratulations!");
                    Console.WriteLine($"You successfully changed the email to {Person.Email}");
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.WriteLine("\nPress [ENTER] to continue");
                    Console.ReadLine();
                }
            }
        }

        public static void ChangePhoneNumber(bool confirmation)
        {
            bool confirmed;
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            if (confirmation)
            {
                Console.WriteLine("Before changing data that can be used as a login, you need to confirm your identity first");
                confirmed = GuestMode.CheckPassword(PersonAuthorizable);
            }
            else
            {
                confirmed = true;
            }
            if (confirmed)
            {
                Console.WriteLine("Now, you can change the phone number");
                string phoneNumber = EnterPhoneNumber();
                if (phoneNumber != "0")
                {
                    Person.PhoneNumber = phoneNumber;
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("\nCongratulations!");
                    Console.WriteLine($"You successfully changed the phone number to {Person.PhoneNumber}");
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.WriteLine("\nPress [ENTER] to continue");
                    Console.ReadLine();
                }
            }
        }

        public static void ChangePassword(bool confirmation)
        {
            bool confirmed;
            Program.SetSize();
            Program.PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            if (confirmation)
            {
                Console.WriteLine("Before changing the password, you need to confirm your identity first");
                confirmed = GuestMode.CheckPassword(PersonAuthorizable);
            }
            else
            {
                confirmed = true;
            }
            if (confirmed)
            {
                Console.WriteLine("Now, you can change the password");
                string password = EnterPassword();
                if (password != "0")
                {
                    PersonAuthorizable.Password = password;
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine("\nCongratulations!");
                    Console.WriteLine($"You successfully changed the password");
                    Console.ForegroundColor = ConsoleColor.DarkGreen;
                    Console.WriteLine("\nPress [ENTER] to continue");
                    Console.ReadLine();
                }
            }
        }


        public static void PrintInfo(bool showID = true, bool confirmation = false)
        {
            short choice;
            bool parsed;
            Dictionary<short, Action> _infoMenuActions = new Dictionary<short, Action>()
            {
                {1, ChangeFirstName},
                {2, ChangeLastName },
                {3, ChangeBirthday },
                {4, () => ChangeEmail(confirmation) },
                {5, () => ChangePhoneNumber(confirmation) },
                {6, () => ChangePassword(confirmation) }
            };
            const short minPoint = 0;
            short maxPoint = _infoMenuActions.Keys.Max();
            Program.SetSize();
            do
            {
                Program.PrintHeader();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Info about a customer:");
                if (showID)
                    Console.WriteLine($"ID : {Buyer.ID}");
                Console.WriteLine($"Name: {Person.GetFullName()}");
                Console.WriteLine($"BirthDay: {Person.Birthday.ToShortDateString()}");
                Console.WriteLine($"Email: {Person.Email}");
                Console.WriteLine($"Phone number: {Person.PhoneNumber}");
                Console.WriteLine();
                Console.WriteLine("1. Change the first name");
                Console.WriteLine("2. Change the last name");
                Console.WriteLine("3. Change the birthday");
                Console.WriteLine("4. Change the email");
                Console.WriteLine("5. Change the phone number");
                Console.WriteLine("6. Change the password");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("0. Go back to menu");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine($"\nEnter the number {minPoint} - {maxPoint}: ");
                parsed = short.TryParse(Console.ReadLine(), out choice);
                if (!parsed)
                    choice = minPoint - 1;
                try
                {
                    _infoMenuActions[choice]?.Invoke();
                }
                catch (KeyNotFoundException) { }
                Console.ForegroundColor = ConsoleColor.DarkGreen;
            } while (choice != 0);
        }

        public static void ChangeQuantity()
        {
            if (Cart.Length == 0)
            {
                Console.WriteLine("You cart is empty");
                Console.WriteLine("\nPress [ENTER] to go back to menu");
                Console.ReadLine();
                return;
            }
            bool parsed;
            Console.WriteLine("\nEnter the index of the product (0 - cancel)");
            parsed = int.TryParse(Console.ReadLine(), out int index);
            while (!parsed || index < 0 || index > Cart.Length)
            {
                Console.WriteLine("You have entered the wrong index");
                Console.WriteLine("Enter the index of the product (0 - cancel)");
                parsed = int.TryParse(Console.ReadLine(), out index);
            }
            if (index == 0)
                return;
            index--;
            string entered;
            do
            {
                Console.WriteLine("\nEnter the new quantity:");
                entered = Console.ReadLine();
                parsed = int.TryParse(entered, out int quantity);
                if (!parsed || quantity <= 0)
                {
                    Console.WriteLine("You entered the wrong quantity (negative or equal 0)");
                }
                else
                {
                    try
                    {
                        GuestMode.Shop.ShowCase.Add(Cart[index]);
                        GuestMode.Shop.ShowCase.Sell(GuestMode.Shop.ShowCase.Find(Cart[index].product), quantity);
                        Cart.SetQuantityAt(index, quantity);
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("\nCongratulations!");
                        Console.WriteLine($"You successfully changed the quantity of product \"{Cart[index].product.Name}\" to {Cart[index].quantity}");
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        Console.WriteLine("\nPress [ENTER] to go back");
                        Console.ReadLine();
                        entered = "0";
                    }
                    catch (ArgumentException)
                    {
                        Console.WriteLine("Unfortunately, the shop does not have such a quantity of this product.");
                        Console.WriteLine("Please, enter less quantity");
                    }
                }
            } while (entered != "0");
        }

        public static void DeleteProduct()
        {
            if (Cart.Length == 0)
            {
                Console.WriteLine("You cart is empty");
                Console.WriteLine("\nPress [ENTER] to go back to menu");
                Console.ReadLine();
                return;
            }
            bool parsed;
            Console.WriteLine("\nEnter the index of the product (0 - cancel)");
            parsed = int.TryParse(Console.ReadLine(), out int index);
            while (!parsed || index < 0 || index > Cart.Length)
            {
                Console.WriteLine("You have entered the wrong index");
                Console.WriteLine("Enter the index of the product (0 - cancel)");
                parsed = int.TryParse(Console.ReadLine(), out index);
            }
            if (index == 0)
                return;
            index--;
            string entered;
            Console.WriteLine($"\nDo you want really to delete the product \"{Cart[index].product.Name}\" from the cart? [Y/n]");
            entered = Console.ReadLine().Trim().ToLower() + " ";
            while (entered[0] != 'n' && entered[0] != 'y')
            {
                Console.WriteLine("Error. Please, enter Y on n once more");
                Console.WriteLine($"\nDo you really want to delete the product \"{Cart[index].product.Name}\" from the cart? [Y/n]");
                entered = Console.ReadLine().Trim().ToLower() + " ";
            }
            if (entered[0] == 'n')
                return;
            var deleted = Cart[index];
            Cart.DeleteAt(index);
            GuestMode.Shop.ShowCase.Add(deleted);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nCongratulations!");
            Console.WriteLine($"You successfully deleted the product from the cart");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("\nPress [ENTER] to go back to menu");
            Console.ReadLine();
        }

        public static void CustomerClearCart()
        {
            if (Cart.Length == 0)
                return;
            string entered;
            Console.WriteLine($"\nDo you want really to clear the cart? [Y/n]");
            entered = Console.ReadLine().Trim().ToLower() + " ";
            while (entered[0] != 'n' && entered[0] != 'y')
            {
                Console.WriteLine("Error. Please, enter Y on n once more");
                Console.WriteLine($"\nDo you really want to clear the cart? [Y/n]");
                entered = Console.ReadLine().Trim().ToLower() + " ";
            }
            if (entered[0] == 'n')
                return;
            ClearCart();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nCongratulations!");
            Console.WriteLine($"You successfully cleared the the cart");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("\nPress [ENTER] to go back to menu");
            Console.ReadLine();
        }

        public static bool ConfirmOrder()
        {
            if (Cart.Length == 0)
            {
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("\nUnfortunately, you cannot confirm the order because it is empty");
                Console.WriteLine("\nPress [ENTER] to go back to menu");
                Console.ReadLine();
                return false;
            }
            string entered;
            Console.WriteLine($"\nDo you want really to confirm the order? [Y/n]");
            entered = Console.ReadLine().Trim().ToLower() + " ";
            while (entered[0] != 'n' && entered[0] != 'y')
            {
                Console.WriteLine("Error. Please, enter Y on n once more");
                Console.WriteLine($"\nDo you want really to confirm the order? [Y/n]");
                entered = Console.ReadLine().Trim().ToLower() + " ";
            }
            if (entered[0] == 'n')
                return false;
            bool firstTime = true;
            Console.WriteLine("\nEnter the address for delivery: ");
            string address = Console.ReadLine();
            while (!address.IsValidAddress() && (address != "0" || firstTime))
            {
                Console.WriteLine("You have entered the wrong address");
                Console.WriteLine("Enter the address for delivery once more (0 - cancel): ");
                address = Console.ReadLine();
                if (firstTime)
                    firstTime = false;
            }
            if (address == "0")
                return false;
            Cart.Confirm(address, new DateTimeProvider());
            Buyer.AddOrder(Cart);
            Cart = new Order(Buyer);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nCongratulations!");
            Console.WriteLine($"You successfully confirmed the order. It will be delivered to the address: {address}");
            Console.WriteLine($"ID of the order: {Buyer[Buyer.OrdersQuantity - 1].ID}");
            Console.WriteLine($"Total price: {string.Format("{0:F2}", Buyer[Buyer.OrdersQuantity - 1].CalculatePrice())} UAH");
            Console.WriteLine("Please, pay for it in the nearest ATM and wait for approvement of the Administrator");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("\nPress [ENTER] to go back to menu");
            Console.ReadLine();
            return true;
        }

        public static void PrintCart()
        {
            short choice;
            bool parsed;
            const short numberLength = -3;
            const short nameMaxLength = -20;
            const short categoryMaxLength = -20;
            const short priceMaxLength = -15;
            const short totalPriceMaxLength = -15;
            Dictionary<short, Action> _cartMenuActions = new Dictionary<short, Action>()
            {
                {1, PrintShowCase},
                {2, ChangeQuantity },
                {3, DeleteProduct },
                {4, CustomerClearCart },
                {5, () => { bool confirmed = ConfirmOrder(); if (confirmed) choice = 0; } },
            };
            const short minPoint = 0;
            short maxPoint = _cartMenuActions.Keys.Max();
            Program.SetSize();
            do
            {
                Program.PrintHeader();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Your cart:");
                Console.WriteLine($"\n{"№",numberLength}{"Name",nameMaxLength}{"Category",categoryMaxLength}{"Price per 1",priceMaxLength}{"Total price",totalPriceMaxLength}");
                for (int i = 0; i < Cart.Length; i++)
                    Console.WriteLine($"{i + 1,numberLength}{Cart[i].product.Name,nameMaxLength}{Cart[i].product.Category,categoryMaxLength}" +
                        $"{string.Format("{0:F2}", Cart[i].product.Price) + " UAH",priceMaxLength}{string.Format("{0:F2}", Cart[i].product.Price * Cart[i].quantity) + " UAH",totalPriceMaxLength}");
                Console.WriteLine($"Total price for the whole order: {string.Format("{0:F2}", Cart.CalculatePrice()) + " UAH"}");
                Console.WriteLine("\nWhat do you want to do?");
                Console.WriteLine("1. Add a product");
                Console.WriteLine("2. Change quantity");
                Console.WriteLine("3. Delete a product");
                Console.WriteLine("4. Clear the cart");
                Console.WriteLine("5. Confirm the order");
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("0. Go back to product base");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine($"\nEnter the number {minPoint} - {maxPoint}: ");
                parsed = short.TryParse(Console.ReadLine(), out choice);
                if (!parsed)
                    choice = minPoint - 1;
                try
                {
                    _cartMenuActions[choice]?.Invoke();
                }
                catch (KeyNotFoundException) { }
                Console.ForegroundColor = ConsoleColor.DarkGreen;
            } while (choice != 0);
        }

        public static void PrintOrder(IOrder order)
        {
            const short numberLength = -3;
            const short nameMaxLength = -20;
            const short categoryMaxLength = -20;
            const short priceMaxLength = -15;
            const short totalPriceMaxLength = -15;
            Program.SetSize();
                Program.PrintHeader();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine($"ID: {order.ID}");
            Console.WriteLine($"Address: {order.Address}");
            Console.WriteLine($"Status: {order.Status}");
            Console.WriteLine($"Customer: {GetCustomerFullNameByID(order.Customer.ID)}");
            Console.WriteLine($"Date and time: {order.Date}");
                Console.WriteLine("Products:");
                Console.WriteLine($"{"№",numberLength}{"Name",nameMaxLength}{"Category",categoryMaxLength}{"Price per 1",priceMaxLength}{"Total price",totalPriceMaxLength}");
                for (int i = 0; i < order.Length; i++)
                    Console.WriteLine($"{i + 1,numberLength}{order[i].product.Name,nameMaxLength}{order[i].product.Category,categoryMaxLength}" +
                        $"{string.Format("{0:F2}", order[i].product.Price) + " UAH",priceMaxLength}{string.Format("{0:F2}", order[i].product.Price * order[i].quantity) + " UAH",totalPriceMaxLength}");
                Console.WriteLine($"Total price for the whole order: {string.Format("{0:F2}", order.CalculatePrice()) + " UAH"}");
        }

        public static string GetCustomerFullNameByID(int id)
        {
            foreach (var customer in GuestMode.Shop.Customers)
                if (customer.ID == id)
                    return customer.GetFullName();
            return string.Empty;
        }

        public static void PrintOrders()
        {
            short choice;
            bool parsed;
            const short idLength = -4;
            const short productMaxLength = -30;
            const short statusMaxLength = -18;
            const short totalPriceMaxLength = -15;
            const short timeMaxLength = -20;
            Program.SetSize();
            do
            {
                Program.PrintHeader();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Your orders:");
                Console.WriteLine($"\n{"ID",idLength}{"Date and time",timeMaxLength}{"Products",productMaxLength}{"Status", statusMaxLength}{"Total price",totalPriceMaxLength}");
                foreach (var order in Buyer)
                    PrintShortOrderInfo(order, true);
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("\nEnter the ID to show information about, 0 - quit: ");
                parsed = short.TryParse(Console.ReadLine(), out choice);
                if (!parsed)
                    choice = -1;
                foreach (var order in Buyer)
                {
                    if (order.ID == choice)
                    {
                        PrintOrder(order);
                        Console.WriteLine("\nPress [ENTER] to go back to menu");
                        Console.ReadLine();
                        break;
                    }
                }
            } while (choice != 0);
        }

        public static void PrintShortOrderInfo(IOrder order, bool hideCustomer)
        {
            const short maxPrintedProducts = 3;
            const short idLength = -4;
            const short customerMaxLength = -30;
            const short timeMaxLength = -20;
            const short productMaxLength = -30;
            const short statusMaxLength = -18;
            const short totalPriceMaxLength = -15;
            Console.Write($"{order.ID, idLength}");
            if (!hideCustomer)
                Console.Write($"{ GetCustomerFullNameByID(order.Customer.ID),customerMaxLength}");
            Console.Write($"{order.Date,timeMaxLength}");
            bool stop = false;
            string products = "";
            for (int i = 0; i < maxPrintedProducts; i++)
            {
                try
                {
                    products +=$" {order[i].product.Name}:{order[i].quantity},";
                }
                catch (ArgumentOutOfRangeException) 
                {
                    stop = true;
                }
                if (stop)
                    break;
            }
            products = products.Remove(products.Length - 1, 1);
            products = products.Remove(0, 1);
            products += "...";
            Console.Write($"{products,productMaxLength}");
            Console.WriteLine($"{order.Status, statusMaxLength}{string.Format("{0:F2}", order.CalculatePrice() + " UAH"),totalPriceMaxLength}");
        }

        public static void PrintSentOrders()
        {
            short choice;
            bool parsed;
            const short idLength = -4;
            const short productMaxLength = -30;
            const short statusMaxLength = -18;
            const short totalPriceMaxLength = -15;
            const short timeMaxLength = -20;
            List<IOrder> sentOrders;
            Program.SetSize();
            do
            {
                sentOrders = new List<IOrder>();
                foreach (var order in Buyer)
                    if (order.Status == OrderStatuses.Sent)
                        sentOrders.Add(order);
                Program.PrintHeader();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Your sent orders:");
                Console.WriteLine($"\n{"ID",idLength}{"Date and time",timeMaxLength}{"Products",productMaxLength}{"Status",statusMaxLength}{"Total price",totalPriceMaxLength}");
                foreach (var order in sentOrders)
                    PrintShortOrderInfo(order, true);
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("\nEnter the ID to confirm the receivement, 0 - quit: ");
                parsed = short.TryParse(Console.ReadLine(), out choice);
                if (!parsed)
                    choice = -1;
                foreach (var order in sentOrders)
                {
                    if (order.ID == choice)
                    {
                        SetReceivedStatus(order);
                        break;
                    }
                }
            } while (choice != 0);
        }

        public static void SetReceivedStatus(IOrder order) 
        {
            string entered;
            Console.WriteLine($"\nDo you really want to confirm the receivement of the order {order.ID}? [Y/n]");
            entered = Console.ReadLine().Trim().ToLower() + " ";
            while (entered[0] != 'n' && entered[0] != 'y')
            {
                Console.WriteLine("Error. Please, enter Y on n once more");
                Console.WriteLine($"\nDo you really want to confirm the receivement of the order {order.ID}? [Y/n]");
                entered = Console.ReadLine().Trim().ToLower() + " ";
            }
            if (entered[0] == 'n')
                return;
            order.Status = OrderStatuses.Received;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nCongratulations!");
            Console.WriteLine($"You successfully confirmed the receivement of the order {order.ID}");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("\nPress [ENTER] to go back to menu");
            Console.ReadLine();
        }

        public static void PrintNotReceivedOrders()
        {
            short choice;
            bool parsed;
            const short idLength = -4;
            const short productMaxLength = -30;
            const short statusMaxLength = -18;
            const short totalPriceMaxLength = -15;
            const short timeMaxLength = -20;
            List<IOrder> notReceivedOrders;
            Program.SetSize();
            do
            {
                notReceivedOrders = new List<IOrder>();
                foreach (var order in Buyer)
                    if (order.Status < OrderStatuses.Received && order.Status != OrderStatuses.CancelledByAdmin && order.Status != OrderStatuses.CancelledByUser)
                        notReceivedOrders.Add(order);
                Program.PrintHeader();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Your not received orders:");
                Console.WriteLine($"\n{"ID",idLength}{"Date and time",timeMaxLength}{"Products",productMaxLength}{"Status",statusMaxLength}{"Total price",totalPriceMaxLength}");
                foreach (var order in notReceivedOrders)
                    PrintShortOrderInfo(order, true);
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("\nEnter the ID to cancel the order, 0 - quit: ");
                parsed = short.TryParse(Console.ReadLine(), out choice);
                if (!parsed)
                    choice = -1;
                foreach (var order in notReceivedOrders)
                {
                    if (order.ID == choice)
                    {
                        CancelOrder(order);
                        break;
                    }
                }
            } while (choice != 0);
        }

        public static void CancelOrder(IOrder order)
        {
            string entered;
            Console.WriteLine($"\nDo you want really to cancel the order {order.ID}? [Y/n]");
            entered = Console.ReadLine().Trim().ToLower() + " ";
            while (entered[0] != 'n' && entered[0] != 'y')
            {
                Console.WriteLine("Error. Please, enter Y on n once more");
                Console.WriteLine($"\nDo you want really to cancel the order {order.ID}? [Y/n]");
                entered = Console.ReadLine().Trim().ToLower() + " ";
            }
            if (entered[0] == 'n')
                return;
            order.Status = OrderStatuses.CancelledByUser;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nCongratulations!");
            Console.WriteLine($"You successfully cancelled the order {order.ID}");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("\nPress [ENTER] to go back to menu");
            Console.ReadLine();
        }

        public static void PrintHelp()
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Menu: ");
            Console.WriteLine("1. View goods");     // buyable menu
            Console.WriteLine("2. View cart");      // + add a product (buy:opens "View goods"), clear and checkout
            Console.WriteLine("3. View the history of orders");
            Console.WriteLine("4. Approve the order receivement");
            Console.WriteLine("5. Cancel order");
            Console.WriteLine("6. View or edit personal information");  // + change password
            Console.WriteLine("7. Help");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("0. Log out");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
        }
    }
}
