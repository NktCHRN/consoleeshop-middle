﻿using System;
using ShopLogic;
using ShopLogic.Interfaces;
using ShopLogic.HelperClasses;
using System.Text;

namespace ShopConsole
{
    public static class Program
    {
        public static short WindowWidth { get; private set; }

        public static short WindowHeight { get; private set; }

        static void Main(string[] args)
        {
            Console.Title = "ESHOP";
            WindowHeight = 35;
            WindowWidth = 140;
            SetSize();
            PrintHeader();
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("Hello!");
            Console.WriteLine("Please, enter the password for the administrator of your shop: ");
            string password = GetHiddenConsoleInput();
            while (!password.IsValidPassword())
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("Unfortunately, you entered not a valid password");
                Console.WriteLine("Password should: ");
                Console.WriteLine("- have the length of at least 8 characters");
                Console.WriteLine("- have at least one digit");
                Console.WriteLine("- have at least uppercase letter");
                Console.WriteLine("- have at least lowercase letter");
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("Please, enter the password for the administrator of your pawnshop once more: ");
                password = GetHiddenConsoleInput();
            };
            IAdmin admin = new Admin(password);
            IShop shop = new Shop(admin, new CustomList<ICustomer>(), new ProductBase());
            GuestMode.Shop = shop;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\nYou sucessfully set up your shop");
            Console.WriteLine($"Your admin's login: {admin.Login}");
            Console.WriteLine("You may change it later in admin's menu");
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("\nPress [ENTER] to continue");
            Console.ReadLine();
            GuestMode.PrintMenu();
        }

        public static void SetSize()
        {
            const short maxBufferHeight = short.MaxValue - 1;
            Console.SetWindowSize(1, 1);
            Console.SetBufferSize(WindowWidth, (Console.BufferHeight > WindowHeight) ? Console.BufferHeight : maxBufferHeight);
            Console.SetWindowSize(WindowWidth, WindowHeight);
        }

        public static void PrintHeader()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            const string name = "ESHOP";
            Console.WriteLine('\n' + name.PadLeft(WindowWidth / 2 + name.Length / 2) + '\n');
            Console.ResetColor();
        }

        public static string GetHiddenConsoleInput()
        {
            StringBuilder input = new StringBuilder();
            while (true)
            {
                int x = Console.CursorLeft;
                int y = Console.CursorTop;
                ConsoleKeyInfo key = Console.ReadKey(true);
                if (key.Key == ConsoleKey.Enter)
                {
                    Console.WriteLine();
                    break;
                }
                if (key.Key == ConsoleKey.Backspace && input.Length > 0)
                {
                    input.Remove(input.Length - 1, 1);
                    Console.SetCursorPosition(x - 1, y);
                    Console.Write(" ");
                    Console.SetCursorPosition(x - 1, y);
                }
                else if (key.Key != ConsoleKey.Backspace)
                {
                    input.Append(key.KeyChar);
                    Console.Write("*");
                }
            }
            return input.ToString();
        }
    }
}
