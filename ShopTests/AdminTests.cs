﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using ShopLogic;
using ShopLogic.Interfaces;
using Moq;

namespace ShopTests
{
    [TestFixture]
    public class AdminTests
    {

        private static readonly string _testPassword = "Password1";

        private static Admin _testAdmin;

        [SetUp]
        public void Setup()
        {
            _testAdmin = new Admin(_testPassword);
        }

        [Test]
        [TestCase("login")]
        [TestCase("Admin1")]
        public void LoginSetterTest_ValidLogin_SetsLogin(string login)
        {
            // Act
            _testAdmin.Login = login;

            // Assert
            Assert.AreEqual(login, _testAdmin.Login, "Property does not sets login");
        }

        [Test]
        [TestCase("lo gin")]
        [TestCase("0")]
        [TestCase("Admin   1")]
        [TestCase("")]
        [TestCase("         ")]
        public void LoginSetterTest_NotValidLogin_ThrowsFormatException(string login)
        {
            // Arrange
            var expectedEx = typeof(System.FormatException);

            // Act
            Exception actualEx = Assert.Catch(() => _testAdmin.Login = login);

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Property login's setter should throw {expectedEx.GetType()} if given login contains white spaces");
        }

        [Test]
        [TestCase(null)]
        public void LoginSetterTest_NullOrEmptyLogin_ThrowsArgumentNullException(string login)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => _testAdmin.Login = login);

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Property login's setter should throw {expectedEx.GetType()} if given login is null or empty");
        }

        [Test]
        [TestCase("Admin1Psw")]
        [TestCase("Admin1psw")]
        [TestCase("admin1Psw")]
        [TestCase("A1111111w")]
        [TestCase("AdMIN1PSW")]
        [TestCase("adsa^%*#!@e802q3uASWJA;sajO213@#@$2")]
        public void PasswordSetterTest_ValidLogin_SetsPassword(string password)
        {
            // Act
            _testAdmin.Password = password;

            // Assert
            Assert.IsTrue(_testAdmin.CheckPassword(password), "Property does not sets password");
        }

        [Test]
        [TestCase("Psword1")]
        [TestCase("password")]
        [TestCase("Password")]
        [TestCase("123312432")]
        [TestCase("123312432pass")]
        [TestCase("123312432PASS")]
        [TestCase("")]
        public void PasswordSetterTest_NotValidPassword_ThrowsFormatException(string password)
        {
            // Arrange
            var expectedEx = typeof(System.FormatException);

            // Act
            Exception actualEx = Assert.Catch(() => _testAdmin.Password = password);

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Property password's setter should throw {expectedEx.GetType()} if given password contains white spaces, it length is smaller than 8 or it does not contain at least one uppercase letter, one lowercase letter and one digit");
        }

        [Test]
        [TestCase(null)]
        public void PasswordSetterTest_NullPassword_ThrowsArgumentNullException(string password)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => _testAdmin.Password = password);

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Property password's setter should throw {expectedEx.GetType()} if given password is null or empty");
        }

        [Test]
        public void CheckLoginTest_RealLogin_ReturnsTrue()
        {
            // Arrange
            var login = _testAdmin.Login;

            // Act
            var actual = _testAdmin.CheckLogin(login);

            // Assert
            Assert.IsTrue(actual, "Method CheckLogin does not checks the login");
        }

        [Test]
        public void CheckLoginTest_NotALogin_ReturnsFalse()
        {
            // Arrange
            var login = "Not a login";

            // Act
            var actual = _testAdmin.CheckLogin(login);

            // Assert
            Assert.IsFalse(actual, "Method CheckLogin does not checks the login");
        }

        [Test]
        public void CheckLoginTest_NullLogin_ThrowsArgumentNullException()
        {
            // Arrange
            string login = null;
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => _testAdmin.CheckLogin(login));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method CheckLogin should throw {expectedEx.GetType()} if the potential login (passed parameter) is null");
        }

        [Test]
        public void CheckPasswordTest_RealPassword_ReturnsTrue()
        {
            // Arrange
            var login = _testPassword;

            // Act
            var actual = _testAdmin.CheckPassword(login);

            // Assert
            Assert.IsTrue(actual, "Method CheckPassword does not checks the password");
        }

        [Test]
        public void CheckPasswordTest_NotAPassword_ReturnsFalse()
        {
            // Arrange
            var password = "NotAPass1";

            // Act
            var actual = _testAdmin.CheckPassword(password);

            // Assert
            Assert.IsFalse(actual, "Method CheckPassword does not checks the password");
        }

        [Test]
        public void CheckLoginTest_NullPassword_ThrowsArgumentNullException()
        {
            // Arrange
            string password = null;
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => _testAdmin.CheckPassword(password));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method CheckPassword should throw {expectedEx.GetType()} if the potential password (passed parameter) is null");
        }

        [Test]
        [TestCase(1)]
        [TestCase(5)]
        public void AddOrderTest_CorrectOrder_ReturnsOrderWithTheSameReference(int quantity)
        {
            // Arrange
            var orderMock = new Mock<IOrder>();
            orderMock.Setup(order => order.Customer.ID).Returns(_testAdmin.ID);
            orderMock.Setup(order => order.IsConfirmed).Returns(true);
            var testOrder = orderMock.Object;

            // Act
            for (int i = 0; i < quantity; i++)
                _testAdmin.AddOrder(testOrder);

            // Assert
            Assert.AreEqual(quantity, _testAdmin.OrdersQuantity, "Method Add does not changes the quantity of orders");
            for (int i = 0; i < quantity; i++)
            {
                Assert.AreEqual(testOrder, _testAdmin[i], "Method Add did not add right order");
                Assert.AreSame(testOrder, _testAdmin[i], "Method Add did not add order with the same reference");
            }
        }

        [Test]
        public void AddOrderTest_AnotherCustomerID_ThrowsArgumentException()
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentException);
            var orderMock = new Mock<IOrder>();
            orderMock.Setup(order => order.Customer.ID).Returns(-1);
            orderMock.Setup(order => order.IsConfirmed).Returns(true);
            var testOrder = orderMock.Object;

            // Act
            Exception actualEx = Assert.Catch(() => _testAdmin.AddOrder(testOrder));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method AddOrder should throw {expectedEx.GetType()} if the order has another customer's id");
        }

        [Test]
        public void AddOrderTest_NotConfirmed_ThrowsArgumentException()
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentException);
            var orderMock = new Mock<IOrder>();
            orderMock.Setup(order => order.Customer.ID).Returns(_testAdmin.ID);
            orderMock.Setup(order => order.IsConfirmed).Returns(false);
            var testOrder = orderMock.Object;

            // Act
            Exception actualEx = Assert.Catch(() => _testAdmin.AddOrder(testOrder));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method AddOrder should throw {expectedEx.GetType()} if the order is not confirmed");
        }

        [Test]
        public void AddOrderTest_NullOrder_ThrowsArgumentNullException()
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);
            IOrder testOrder = null;

            // Act
            Exception actualEx = Assert.Catch(() => _testAdmin.AddOrder(testOrder));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method AddOrder should throw {expectedEx.GetType()} if the order is null");
        }

        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        public void IndexerTest_ReturnsIOrderWithSameRef(int index)
        {
            // Arrange
            var expectedList = new List<IOrder>();
            const int quantity = 5;
            for (int i = 0; i < quantity; i++)
            {
                var orderMock = new Mock<IOrder>();
            orderMock.Setup(order => order.Customer.ID).Returns(_testAdmin.ID);
            orderMock.Setup(order => order.IsConfirmed).Returns(true);
                orderMock.Setup(order => order.ID).Returns(i + 1);
                var testOrder = orderMock.Object;
                expectedList.Add(testOrder);
                _testAdmin.AddOrder(testOrder);
            };
            var expected = expectedList[index];

            // Act
            var actual = _testAdmin[index];

            // Assert
            Assert.AreEqual(expected, actual,
                            message: "Indexer does not return right value");
            Assert.AreSame(expected, actual,
                            message: "Order's references should be the same");
        }

        [Test]
        [TestCase(-3)]
        [TestCase(-1)]
        [TestCase(5)]
        [TestCase(10)]
        public void IndexerTest_ThrowsArgumentOutOfRangeException(int index)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            IOrder func() => _testAdmin[index];
            Exception actualEx = Assert.Catch(() => func());

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Indexer should throw {expectedEx.GetType()} if index is out of range");
        }

        [Test]
        [TestCase(OrderStatuses.New)]
        [TestCase(OrderStatuses.Sent)]
        public void CancelOrderTest_CorrectArguments_ReturnsTrue(OrderStatuses status)
        {
            // Arrange
            IOrder order = new Order(_testAdmin)
            {
                (new Mock<IProduct>().Object, 1)
            };
            order.Confirm("test Address, 1", new Mock<IDateTimeProvider>().Object);
            order.Status = status;
            _testAdmin.AddOrder(order);
            const int orderIndex = 0;

            // Act
            var actual = _testAdmin.CancelOrder(orderIndex);

            // Assert
            Assert.IsTrue(actual, "Method CancelOrder should return true if the order status is not Received or higher");
            Assert.AreEqual(OrderStatuses.CancelledByUser, _testAdmin[orderIndex].Status, "Method CancelOrder should cancel the order if the order status is not Received or higher");
        }

        [Test]
        [TestCase(OrderStatuses.Received)]
        [TestCase(OrderStatuses.Ended)]
        public void CancelOrderTest_ReceivedOrHigherStatus_ReturnsFalse(OrderStatuses oldStatus)
        {
            // Arrange
            IOrder order = new Order(_testAdmin)
            {
                (new Mock<IProduct>().Object, 1)
            };
            order.Confirm("test Address, 1", new Mock<IDateTimeProvider>().Object);
            order.Status = oldStatus;
            _testAdmin.AddOrder(order);
            _testAdmin.AddOrder(order);
            const int orderIndex = 0;

            // Act
            var actual = _testAdmin.CancelOrder(orderIndex);

            // Assert
            Assert.IsFalse(actual, "Method CancelOrder should return false if the order status is Received or higher");
            Assert.AreEqual(oldStatus, _testAdmin[orderIndex].Status, "Method CancelOrder should not cancel the order if the order status is Received or higher");
        }

        [Test]
        public void EnumeratorTest_ReturnsIOrderWithTheSameReference()
        {
            // Arrange
            var expectedList = new List<IOrder>();
            const int quantity = 5;
            for (int i = 0; i < quantity; i++)
            {
                var orderMock = new Mock<IOrder>();
                orderMock.Setup(order => order.Customer.ID).Returns(_testAdmin.ID);
                orderMock.Setup(order => order.IsConfirmed).Returns(true);
                orderMock.Setup(order => order.ID).Returns(i + 1);
                var testOrder = orderMock.Object;
                expectedList.Add(testOrder);
                _testAdmin.AddOrder(testOrder);
            };
            var actualList = new List<IOrder>();

            // Act
            foreach (var item in _testAdmin)
                actualList.Add(item);

            // Assert
            for (int i = 0; i < _testAdmin.OrdersQuantity; i++)
            {
                Assert.AreEqual(expectedList[i], actualList[i], "Enumerator returns incorrect value");
                Assert.AreSame(expectedList[i], actualList[i], "Enumerator should return the order with the same reference");
            }
        }

        [Test]
        [TestCase(OrderStatuses.CancelledByAdmin)]
        [TestCase(OrderStatuses.Sent)]
        [TestCase(OrderStatuses.Ended)]
        [TestCase(OrderStatuses.PaymentReceived)]
        [TestCase(OrderStatuses.New)]
        [TestCase(OrderStatuses.Received)]
        public void SetStatusTest_CorrectStatus_SetsStatusAndInvokesEvent(OrderStatuses status)
        {
            // Arrange
            IProduct product = new Product("prod", Categories.Appliances, "desc", 77.5m);
            IOrder order = new Order(new Mock<IBuyer>().Object)
            {
                (product, 1)
            };
            IOrder fakeOrder = new Order(new Mock<IBuyer>().Object)
            {
                (product, 1)
            };
            fakeOrder.Confirm("test Address, 1", new Mock<IDateTimeProvider>().Object);
            fakeOrder.Status = status;
            order.Confirm("test Address, 1", new Mock<IDateTimeProvider>().Object);
            (object, StatusEventArgs) expectedEvent = (_testAdmin, new StatusEventArgs(fakeOrder, order.Status, status));
            (object, StatusEventArgs) actualEvent = default;
            _testAdmin.StatusSetEvent += (sender, args) => actualEvent = (sender, args);

            // Act
            _testAdmin.SetStatus(order, status);

            // Assert
            Assert.AreEqual(status, order.Status, "Method SetStatus should set the status if the argument is correct");
            Assert.IsTrue(actualEvent != default, "Method SetStatus should invoke the SetStatusEvent if the argument is correct");
            Assert.AreEqual(expectedEvent.Item1, actualEvent.Item1, "Method SetStatus invoked the event with the wrong arguments (sender)");
            Assert.AreEqual(expectedEvent.Item2.Order, actualEvent.Item2.Order, "Method SetStatus invoked the event with the wrong arguments (StatusEventArgs:order)");
            Assert.AreEqual(expectedEvent.Item2.LastStatus, actualEvent.Item2.LastStatus, "Method SetStatus invoked the event with the wrong arguments (StatusEventArgs:LastStatus)");
            Assert.AreEqual(expectedEvent.Item2.NewStatus, actualEvent.Item2.NewStatus, "Method SetStatus invoked the event with the wrong arguments (StatusEventArgs:NewStatus)");
        }

        [Test]
        public void SetStatusTest_CancelledByUserStatus_ThrowsArgumentException()
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentException);
            IOrder order = new Order(new Mock<IBuyer>().Object)
            {
                (new Mock<IProduct>().Object, 1)
            };
            order.Confirm("test Address, 1", new Mock<IDateTimeProvider>().Object);

            // Act
            Exception actualEx = Assert.Catch(() => _testAdmin.SetStatus(order, OrderStatuses.CancelledByUser));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method SetStatus should throw {expectedEx.GetType()} if the new status is CancelledByUser");
        }

        [Test]
        public void SetStatusTest_NullOrder_ThrowsArgumentNullException()
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);
            IOrder order = null;

            // Act
            Exception actualEx = Assert.Catch(() => _testAdmin.SetStatus(order, OrderStatuses.CancelledByAdmin));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method SetStatus should throw {expectedEx.GetType()} if the new status is null");
        }

    }
}
