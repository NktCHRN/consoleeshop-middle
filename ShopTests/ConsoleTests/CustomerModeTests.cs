﻿using NUnit.Framework;
using ShopLogic.Interfaces;
using ShopConsole;
using Moq;
using ShopLogic.HelperClasses;

namespace ShopTests.ConsoleTests
{
    [TestFixture]
    public class CustomerModeTests
    {
        public CustomList<ICustomer> customers;

        public static readonly string[] names = new string[] { "Nick Hazzard", "Macey Hawes", "Javan Read", "Rylan Stone", "Niamh Henry" };

        [SetUp]
        public void Setup()
        {
            var shopMock = new Mock<IShop>();
            const int customersArraySize = 5;
            customers = new CustomList<ICustomer>();
            for (int i = 0; i < customersArraySize; i++)
            {
                var customerMock = new Mock<ICustomer>();
                customerMock.Setup(customer => customer.ID).Returns(i + 1);
                customerMock.Setup(customer => customer.GetFullName()).Returns(names[i]);
                customers.Add(customerMock.Object);
            }
            shopMock.Setup(shop => shop.Customers).Returns(customers);
            GuestMode.Shop = shopMock.Object;
        }

        [Test]
        [TestCase(1)]
        [TestCase(2)]
        [TestCase(3)]
        [TestCase(4)]
        [TestCase(5)]
        public void GetCustomerFullNameByID_Found_ReturnsName(int id)
        {
            // Arrange
            var expected = names[id - 1];

            // Act
            var actual = CustomerMode.GetCustomerFullNameByID(id);

            // Assert
            Assert.AreEqual(expected, actual, "Method GetCustomerFullNameByID returns wrong customer's name if the customer was found");
        }

        [Test]
        [TestCase(-5)]
        [TestCase(-1)]
        [TestCase(0)]
        [TestCase(6)]
        [TestCase(10)]
        public void GetCustomerFullNameByID_NotFound_ReturnsEmptyString(int id)
        {
            // Arrange
            var expected = string.Empty;

            // Act
            var actual = CustomerMode.GetCustomerFullNameByID(id);

            // Assert
            Assert.AreEqual(expected, actual, "Method GetCustomerFullNameByID does not return empty string if the customer was not found");
        }
    }
}
