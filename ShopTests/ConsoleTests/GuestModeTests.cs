﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using ShopLogic;
using ShopLogic.Interfaces;
using ShopConsole;
using System.Linq;

namespace ShopTests.ConsoleTests
{
    [TestFixture]
    public class GuestModeTests
    {

        private (IProduct, int)[] _testArray;

        [SetUp]
        public void Setup()
        {
            (IProduct, int)[] array = { (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
                (new Product("Barbell", Categories.SportEquipment, "Metal vulture for a barbell", 350m), 8),
            (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
            (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2),
            (new Product("Pen", Categories.Stationery, "A blue ball point pen", 15m), 0),
            (new Product("Knife", Categories.Appliances, "A luxorious multifunctional swiss knife", 350m), 3)};
            _testArray = array;
        }

        private static IEnumerable<TestCaseData> FindTest_NotNullString_TestCases
        {
            get
            {
                yield return new TestCaseData("b", new (IProduct, int)[]{ (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
                (new Product("Barbell", Categories.SportEquipment, "Metal vulture for a barbell", 350m), 8),
            (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1)});
                yield return new TestCaseData("note", new (IProduct, int)[] { (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1) });
                yield return new TestCaseData("PEN", new (IProduct, int)[] { (new Product("Pen", Categories.Stationery, "A blue ball point pen", 15m), 0) });
                yield return new TestCaseData("dUMBBEl", new (IProduct, int)[] { (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10) });
                yield return new TestCaseData("Dress", new (IProduct, int)[] { });
            }
        }

        [Test]
        [TestCaseSource("FindTest_NotNullString_TestCases")]
        public void FindTest_NotNullString_ReturnsNewBaseWithResults(string find, (IProduct, int)[] expected)
        {
            // Act
            (IProduct, int)[] actual = GuestMode.Find(_testArray, find).ToArray();

            // Assert
            for (int i = 0; i < expected.Length; i++)
                Assert.AreEqual(expected[i], actual[i], "Method Find returns incorrect ProductBase");
        }

        [Test]
        [TestCase(null)]
        public void FindTest_NullString_ThrowsArgumentNullException(string find)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => GuestMode.Find(_testArray, find));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method Find should throw {expectedEx.GetType()} if string find is null");
        }

        [Test]
        [TestCase("String")]
        public void FindTest_NullIEnumerable_ThrowsArgumentNullException(string find)
        {
            // Arrange
            IEnumerable<(IProduct, int)> products = null;
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => GuestMode.Find(products, find));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method Find should throw {expectedEx.GetType()} if IEnumerable products is null");
        }

        [Test]
        [TestCase(ProductSortingTypes.PriceAscending)]
        [TestCase(ProductSortingTypes.PriceDescending)]
        public void GetSortedTest_NullIEnumerable_ThrowsArgumentNullException(ProductSortingTypes order)
        {
            // Arrange
            IEnumerable<(IProduct, int)> products = null;
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => GuestMode.GetSorted(products, order));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method GetSorted should throw {expectedEx.GetType()} if IEnumerable products is null");
        }

        [Test]
        public void GetOnSaleTest()
        {
            // Arrange
            (IProduct, int)[] expected = _testArray.Where(product => product.Item2 > 0).ToArray();

            // Act
            (IProduct, int)[] actual = GuestMode.GetOnSale(_testArray).ToArray();

            // Assert
            for (int i = 0; i < expected.Length; i++)
                Assert.AreEqual(expected[i], actual[i], "Method GetOnSale returns incorrect ProductBase");
        }

        [Test]
        public void GetOnSaleTest_NullIEnumerable_ThrowsArgumentNullException()
        {
            // Arrange
            IEnumerable<(IProduct, int)> products = null;
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => GuestMode.GetOnSale(products));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method GetOnSale should throw {expectedEx.GetType()} if IEnumerable products is null");
        }

        [Test]
        [TestCase(Categories.SportEquipment)]
        [TestCase(Categories.Stationery)]
        [TestCase(Categories.HouseholdChemicals)]
        public void GetFilteredTest(Categories filter)
        {
            // Arrange
            (IProduct, int)[] expected = _testArray.Where(product => product.Item1.Category == filter).ToArray();

            // Act
            (IProduct, int)[] actual = GuestMode.GetFiltered(_testArray, filter).ToArray();

            // Assert
            for (int i = 0; i < expected.Length; i++)
                Assert.AreEqual(expected[i], actual[i], "Method GetOnSale returns incorrect ProductBase");
        }

        [Test]
        [TestCase(Categories.SportEquipment)]
        [TestCase(Categories.Stationery)]
        [TestCase(Categories.HouseholdChemicals)]
        public void GetFilteredTest_NullIEnumerable_ThrowsArgumentNullException(Categories filter)
        {
            // Arrange
            IEnumerable<(IProduct, int)> products = null;
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => GuestMode.GetFiltered(products, filter));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method GetFiltered should throw {expectedEx.GetType()} if IEnumerable products is null");
        }

        [Test]
        public void GetSortedTest_ReturnsSortedBaseAscending()
        {
            // Arrange
            var order = ProductSortingTypes.PriceAscending;
            var expected = new (IProduct, int)[] { (new Product("Pen", Categories.Stationery, "A blue ball point pen", 15m), 0),
                        (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                        (new Product("Barbell", Categories.SportEquipment, "Metal vulture for a barbell", 350m), 8),
                        (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
                        (new Product("Knife", Categories.Appliances, "A luxorious multifunctional swiss knife", 350m), 3),
                        (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
                        (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
                        (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2) };

            // Act
            (IProduct, int)[] actual = GuestMode.GetSorted(_testArray, order).ToArray();

            // Assert
            for (int i = 0; i < expected.Length; i++)
                Assert.AreEqual(expected[i], actual[i], "Method GetSorted returns incorrect ProductBase");
        }

        [Test]
        public void GetSortedTest_ReturnsSortedBaseDescending()
        {
            // Arrange
            var order = ProductSortingTypes.PriceDescending;
            var expected = new (IProduct, int)[] {
                    (new Product("Gold ingot", Categories.Jewelry, "A 100g bank gold ingot", 100000m), 2),
                    (new Product("Notebook", Categories.Electronics, "A powerful gaming notebook", 30000m), 1),
                    (new Product("Jacket", Categories.Clothes, "A luxorious black jacket for men", 5000m), 5),
                    (new Product("Barbell", Categories.SportEquipment, "Metal vulture for a barbell", 350m), 8),
                    (new Product("Dumbbells", Categories.SportEquipment, "Two 5 kg metal dumbbells", 350m), 10),
                    (new Product("Knife", Categories.Appliances, "A luxorious multifunctional swiss knife", 350m), 3),
                    (new Product("Cabbage", Categories.Food, "Leafy cabbage from Ukraine", 77.55m), 20),
                    (new Product("Pen", Categories.Stationery, "A blue ball point pen", 15m), 0) };

            // Act
            (IProduct, int)[] actual = GuestMode.GetSorted(_testArray, order).ToArray();

            // Assert
            for (int i = 0; i < expected.Length; i++)
                Assert.AreEqual(expected[i], actual[i], "Method GetSorted returns incorrect ProductBase");
        }
    }
}
