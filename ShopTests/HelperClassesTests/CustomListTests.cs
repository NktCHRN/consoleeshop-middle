﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using ShopLogic.HelperClasses;

namespace ShopTests.HelperClassesTests
{
    [TestFixture]
    public class CustomListTests
    {
        private static int?[] _testArray;

        private static CustomList<int?> _testCustomList;

        [SetUp]
        public void Setup()
        {
            _testArray = new int?[]{ 77, 22, 34, 75, 100, 23, 5400, 33};
            _testCustomList = new CustomList<int?>(_testArray);
        }

        private static IEnumerable<TestCaseData> AddTest_CorrectItem_TestCases
        {
            get
            {
                yield return new TestCaseData(new CustomList<int?>(new int?[] { 77, 22, 34, 75, 100, 23, 5400, 33 }), 85);
                yield return new TestCaseData(new CustomList<int?>(new int?[] { 77, 22, 34, 75, 100, 23, 5400, 33 }), 34);
                yield return new TestCaseData(new CustomList<int?>(new int?[] { 77, 22, 34, 75, 100, 23, 5400, 33 }), 0);
                yield return new TestCaseData(new CustomList<int?>(new int?[] { 77, 22, 34, 75, 100, 23, 5400, 33 }), -1);
                yield return new TestCaseData(new CustomList<int?>(new int?[] { 77, 22, 34, 75, 100, 23, 5400, 33 }), -30);
                yield return new TestCaseData(new CustomList<int?>(), 87);
            }
        }

        private static IEnumerable<TestCaseData> AddTest_NullItem_TestCases
        {
            get
            {
                yield return new TestCaseData(new CustomList<int?>(new int?[] { 77, 22, 34, 75, 100, 23, 5400, 33 }), null);
                yield return new TestCaseData(new CustomList<int?>(), null);
            }
        }

        [Test]
        public void CountTest_ReturnsRealCount()
        {
            // Arrange
            var expected = _testArray.Length;

            // Act
            var actual = _testCustomList.Count;

            // Assert
            Assert.AreEqual(expected, actual, "Property Count returns wrong number of elements in the array");
        }

        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(4)]
        [TestCase(6)]
        [TestCase(7)]
        public void IndexerTest_ReturnsElementOnIndex(int index)
        {
            // Arrange
            var expected = _testArray[index];

            // Act
            var actual = _testCustomList[index];

            // Assert
            Assert.AreEqual(expected, actual,
                            message: "Indexer does not return right value");
        }

        [Test]
        [TestCase(-3)]
        [TestCase(-1)]
        [TestCase(8)]
        [TestCase(10)]
        public void IndexerTest_ThrowsArgumentOutOfRangeException(int index)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            int? func() => _testCustomList[index];
            Exception actualEx = Assert.Catch(() => func());

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Indexer should throw {expectedEx.GetType()} if index is out of range");
        }

        [Test]
        [TestCase(0)]
        [TestCase(1)]
        [TestCase(4)]
        [TestCase(6)]
        [TestCase(7)]
        public void DeleteAtTest_CorrectIndex_ReturnsElementOnIndex(int index)
        {
            // Arrange
            var expectedSize = _testArray.Length - 1;
            var expected = new int?[expectedSize];
            int correction = 0;
            for (int i = 0; i < _testArray.Length; i++)
            {
                if (i != index)
                    expected[i - correction] = _testArray[i];
                else
                    correction++;
            }

            // Act
            _testCustomList.DeleteAt(index);

            // Assert
            Assert.AreEqual(expectedSize, _testCustomList.Count,
                message: "Method DeleteAt does not changed the size of the list");
            for (int i = 0; i < expectedSize; i++)
            {
                Assert.AreEqual(expected[i], _testCustomList[i],
                message: $"Method DeleteAt deleted element at another index. Problem at index: {i} (in new list)");
            }
        }

        [Test]
        [TestCase(-3)]
        [TestCase(-1)]
        [TestCase(8)]
        [TestCase(10)]
        public void DeleteAtTest_IncorrectIndex_ThrowsArgumentOutOfRangeException(int index)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentOutOfRangeException);

            // Act
            Exception actualEx = Assert.Catch(() => _testCustomList.DeleteAt(index));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method DeleteAt should throw {expectedEx.GetType()} if index is out of range");
        }

        [Test]
        [TestCase(77)]
        [TestCase(34)]
        [TestCase(33)]
        public void FindTest_InOrder_ReturnsCorrectIndex(int number)
        {
            // Arrange
            var expected = -1;
            for (int i = 0; i < _testArray.Length; i++)
            {
                if (_testArray[i].Equals(number))
                {
                    expected = i;
                    break;
                }
            }

            // Act
            var actual = _testCustomList.Find(number);

            // Assert
            Assert.AreEqual(expected, actual,
                message: "Method Find returns incorrect index");
        }

        [Test]
        [TestCase(777)]
        [TestCase(30)]
        [TestCase(3)]
        public void FindTest_NotInOrder_ReturnsMinusOne(int number)
        {
            // Arrange
            const int expected = -1;

            // Act
            var actual = _testCustomList.Find(number);

            // Assert
            Assert.AreEqual(expected, actual,
                message: "Method Find returns index for a product that was not found");
        }

        [Test]
        [TestCaseSource("AddTest_CorrectItem_TestCases")]
        public void AddTest_CorrectItem_AddsItem(CustomList<int?> list, int? number)
        {
            //Arrange
            int expectedLength = list.Count + 1;

            // Act
            list.Add(number);

            // Assert
            Assert.AreEqual(expectedLength, list.Count, "Method Add does not increases the size of the list");
            Assert.AreEqual(number, list[^1], "Method Add does not adds the item");
        }

        [Test]
        [TestCaseSource("AddTest_NullItem_TestCases")]
        public void AddTest_NullItem_ThrowsArgumentNullException(CustomList<int?> list, int? item)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => list.Add(item));

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method Add should throw {expectedEx.GetType()} if parameter item is null");
        }

        [Test]
        public void EnumeratorTest_ReturnsEqualItems()
        {
            // Arrange
            List<int?> actualList = new List<int?>();

            // Act
            foreach (var item in _testCustomList)
                actualList.Add(item);

            // Assert
            for (int i = 0; i < _testArray.Length; i++)
                Assert.AreEqual(_testArray[i], actualList[i], "Enumerator returns incorrect value");
        }
    }
}
