﻿using System;
using NUnit.Framework;
using ShopLogic.HelperClasses;

namespace ShopTests.HelperClassesTests
{
    [TestFixture]
    public class StringValidatorExtensionTests
    {
        [Test]
        [TestCase("Nikita")]
        [TestCase("Tim")]
        [TestCase("John")]
        [TestCase("Vladislav")]
        [TestCase("Li")]
        [TestCase("Cooper")]
        [TestCase("Ostrohradsky")]
        [TestCase("Berners-Lee")]
        public void IsValidNameTest_ValidName_ReturnsTrue(string potentialName)
        {
            // Act
            var actual = potentialName.IsValidName();

            // Assert
            Assert.IsTrue(actual, "Method IsValidName should return true if the name is valid");
        }

        [Test]
        [TestCase("nikita")]
        [TestCase("T")]
        [TestCase("y")]
        [TestCase("   Vladislav")]
        [TestCase("L1")]
        [TestCase("")]
        [TestCase("     ")]
        [TestCase("Co0per")]
        [TestCase("$idorov")]
        [TestCase("So%enkov")]
        public void IsValidNameTest_NotValidName_ReturnsFalse(string potentialName)
        {
            // Act
            var actual = potentialName.IsValidName();

            // Assert
            Assert.IsFalse(actual, "Method IsValidName should return false if the name is not valid");
        }

        [Test]
        [TestCase(null)]
        public void IsValidNameTest_NullName_ThrowsArgumentNullException(string potentialName)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => potentialName.IsValidName());

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method IsValidName should throw {expectedEx.GetType()} if the name is null");
        }

        [Test]
        [TestCase("4nick@gmail.com")]
        [TestCase("nick@hotmail.com")]
        [TestCase("423@ukr.net")]
        [TestCase("4$3@ukr.net")]
        [TestCase("4$3@ukr.kyiv.ua")]
        [TestCase("temp1.te@yandex.ru")]
        [TestCase(@"Fred\ Bloggs @example.com")]
        [TestCase(@"Joe.\\Blow @example.com")]
        [TestCase("\"Fred Bloggs\"@example.com")]
        [TestCase(@"customer/department= shipping@example.com")]
        [TestCase(@"$A12345 @example.com")]
        [TestCase(@"!def!xyz%abc @example.com")]
        [TestCase(@"_somename@example.com")]
        public void IsValidEmailTest_ValidEmail_ReturnsTrue(string potentialEmail)
        {
            // Act
            var actual = potentialEmail.IsValidEmail();

            // Assert
            Assert.IsTrue(actual, "Method IsValidEmail should return true if the e-mail is valid");
        }

        [Test]
        [TestCase("nikita")]
        [TestCase("temp1@")]
        [TestCase("temp1@.")]
        [TestCase("temp1@eee.")]
        [TestCase("temp1.")]
        [TestCase("")]
        [TestCase("     ")]
        public void IsValidEmailTest_NotValidEmail_ReturnsFalse(string potentialEmail)
        {
            // Act
            var actual = potentialEmail.IsValidEmail();

            // Assert
            Assert.IsFalse(actual, "Method IsValidEmail should return false if the e-mail is not valid");
        }

        [Test]
        [TestCase(null)]
        public void IsValidEmailTest_NullEmail_ThrowsArgumentNullException(string potentialEmail)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => potentialEmail.IsValidEmail());

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method IsValidEmail should throw {expectedEx.GetType()} if the e-mail is null");
        }

        [Test]
        [TestCase("380682777777")]
        [TestCase("+380682777777")]
        [TestCase("+38-068-277-77-77")]
        [TestCase("+38 068 277 77 77")]
        [TestCase("+38 068 277-77-77")]
        [TestCase("+38 068 277-77-77")]
        [TestCase("+(38 068) 277-77-77")]
        [TestCase("+71234567890")]
        [TestCase("+15852826539")]
        [TestCase("+3584573975697")]
        public void IsValidPhoneNumberTest_ValidPhoneNumber_ReturnsTrue(string potentialPhoneNumber)
        {
            // Act
            var actual = potentialPhoneNumber.IsValidPhoneNumber();

            // Assert
            Assert.IsTrue(actual, "Method IsValidPhoneNumber should return true if the phone number is valid");
        }

        [Test]
        [TestCase("n")]
        [TestCase("hello")]
        [TestCase("+++380682777777")]
        [TestCase("+---------")]
        [TestCase("---------")]
        [TestCase("+380++683777777")]
        [TestCase("+380n82777777")]
        [TestCase("+380#82777777")]
        [TestCase("+3806n82777777")]
        [TestCase("+3806$82777777")]
        [TestCase("+000000000000")]
        [TestCase("000000000000")]
        [TestCase("555")]
        [TestCase("911")]
        [TestCase("     ")]
        [TestCase("")]
        public void IsValidPhoneNumberTest_NotValidPhoneNumber_ReturnsFalse(string potentialPhoneNumber)
        {
            // Act
            var actual = potentialPhoneNumber.IsValidPhoneNumber();

            // Assert
            Assert.IsFalse(actual, "Method IsValidPhoneNumber should return false if the phone number is not valid");
        }

        [Test]
        [TestCase(null)]
        public void IsValidPhoneNumberTest_NullPhoneNumber_ThrowsArgumentNullException(string potentialPhoneNumber)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => potentialPhoneNumber.IsValidPhoneNumber());

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method IsValidPhoneNumber should throw {expectedEx.GetType()} if the phone number is null");
        }

        [Test]
        [TestCase("City Kyiv, Shevchenka 1")]
        [TestCase("City Lviv, Virmenska street 8")]
        public void IsValidAddressTest_ValidAddress_ReturnsTrue(string potentialAddress)
        {
            // Act
            var actual = potentialAddress.IsValidAddress();

            // Assert
            Assert.IsTrue(actual, "Method IsValidAddress should return true if the address is valid");
        }

        [Test]
        [TestCase("1")]
        [TestCase("a")]
        [TestCase("88")]
        [TestCase("888")]
        [TestCase("8888")]
        [TestCase("88  88")]
        [TestCase("he")]
        [TestCase("hello world")]
        [TestCase("./.,")]
        [TestCase("8,8")]
        [TestCase("Hello, world?!")]
        [TestCase("h8")]
        [TestCase("")]
        [TestCase("    ")]
        public void IsValidAddressTest_NotValidAddress_ReturnsFalse(string potentialAddress)
        {
            // Act
            var actual = potentialAddress.IsValidAddress();

            // Assert
            Assert.IsFalse(actual, "Method IsValidAddress should return false if the address is not valid");
        }

        [Test]
        [TestCase(null)]
        public void IsValidAddressTest_NullAddress_ThrowsArgumentNullException(string potentialAddress)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => potentialAddress.IsValidAddress());

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method IsValidAddress should throw {expectedEx.GetType()} if the address is null");
        }

        [Test]
        [TestCase("Qwert7Qwert7")]
        [TestCase("Qw1Qw1Qw1")]
        [TestCase("Qw1!@#$#")]
        [TestCase("Hello,world?!1")]
        [TestCase("VeryLongPassword1234567890!@#$%^&*()_+~}{\"|?><:\"\\||\\?/.,;[]=-`~")]
        public void IsValidPasswordTest_ValidPassword_ReturnsTrue(string potentialPassword)
        {
            // Act
            var actual = potentialPassword.IsValidPassword();

            // Assert
            Assert.IsTrue(actual, "Method IsValidPassword should return true if the password is valid");
        }

        [Test]
        [TestCase("1")]
        [TestCase("a")]
        [TestCase("88")]
        [TestCase("888")]
        [TestCase("8888")]
        [TestCase("88      88")]
        [TestCase("88888888")]
        [TestCase("Qw1!@#$")]
        [TestCase("hello world")]
        [TestCase("QwertyQwerty")]
        [TestCase("Qwert7Qwert7 ")]
        [TestCase(" Qwert7Qwert7")]
        [TestCase("Qwert7 Qwert7")]
        [TestCase("QWERTYQWERTY")]
        [TestCase("8werty8werty")]
        [TestCase("qwertyqwerty")]
        [TestCase("Hello, world?!")]
        [TestCase("Hello,world?!")]
        [TestCase("h8")]
        [TestCase("")]
        [TestCase("    ")]
        public void IsValidPasswordTest_NotValidPassword_ReturnsFalse(string potentialPassword)
        {
            // Act
            var actual = potentialPassword.IsValidPassword();

            // Assert
            Assert.IsFalse(actual, "Method IsValidPassword should return false if the password is not valid");
        }

        [Test]
        [TestCase(null)]
        public void IsValidPasswordTest_NullPassword_ThrowsArgumentNullException(string potentialPassword)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => potentialPassword.IsValidPassword());

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method IsValidPassword should throw {expectedEx.GetType()} if the password is null");
        }

        [Test]
        [TestCase("380682777777")]
        [TestCase("+380682777777")]
        [TestCase("+38-068-277-77-77")]
        [TestCase("+38 068 277 77 77")]
        [TestCase("+38 068 277-77-77")]
        [TestCase("+38 068 277-77-77")]
        [TestCase("+(38 068) 277-77-77")]
        public void NormalizePhoneNumberTest_ValidPhoneNumber_ReturnsNormalized(string potentialPassword)
        {
            // Arrange
            var expected = "+380682777777";

            // Act
            var actual = potentialPassword.NormalizePhoneNumber();

            // Assert
            Assert.AreEqual(expected, actual, "Method NormalizePhoneNumber should return normalized phone number if given parameter is not null");
        }

        [Test]
        [TestCase(null)]
        public void NormalizePhoneNumberTest_NullPhoneNumber_ThrowsArgumentNullException(string phoneNumber)
        {
            // Arrange
            var expectedEx = typeof(System.ArgumentNullException);

            // Act
            Exception actualEx = Assert.Catch(() => phoneNumber.NormalizePhoneNumber());

            // Assert
            Assert.AreEqual(expectedEx, actualEx.GetType(),
                            message: $"Method IsValidPassword should throw {expectedEx.GetType()} if given parameter is null");
        }
    }
}
