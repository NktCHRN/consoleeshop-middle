﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace ShopLogic.HelperClasses
{
    public static class StringValidatorExtension
    {
        /// <summary>
        /// Checks whether the string can be a customer's name
        /// </summary>
        /// <param name="name">Potential Name</param>
        /// <returns></returns>
        public static bool IsValidName(this string name)
        {
            if (name == null)
                throw new ArgumentNullException(nameof(name), "Potential name cannot be null");
            return !string.IsNullOrWhiteSpace(name) && name.Length > 1 && char.IsUpper(name[0]) && name.All(symbol => char.IsLetter(symbol) || symbol == '\'' || symbol == '`' || symbol == '-' || symbol == ' ');
        }

        public static bool IsValidEmail(this string email)
        {
            if (email == null)
                throw new ArgumentNullException(nameof(email), "Potential email address cannot be null");
            if (!new EmailAddressAttribute().IsValid(email))
                return false;
            email = email.Remove(0, email.LastIndexOf('@') + 1);
            if (!email.Contains('.') || email[0] == '.' || email[^1] == '.')
                return false;
            for (int i = 0; i < email.Length - 1; i++)
                if (email[i] == '.' && email[i + 1] == '.')
                    return false;
            return true;
        }

        public static bool IsValidPhoneNumber(this string number)
        {
            if (number == null)
                throw new ArgumentNullException(nameof(number), "Potential phone number cannot be null");
            const int phoneNumberMinLength = 7;
            const int phoneNumberMaxLength = 15;
            number = number.NormalizePhoneNumber();
            return number.Length >= phoneNumberMinLength && number.Length <= phoneNumberMaxLength && number.Remove(0, 1).All(symbol => char.IsDigit(symbol)) && !number.Remove(0, 1).All(symbol => symbol == '0');
        }

        public static string NormalizePhoneNumber(this string number)
        {
            if (number == null)
                throw new ArgumentNullException(nameof(number), "Potential phone number cannot be null");
            number = number.Replace(" ", "").Replace("-", "").Replace("(", "").Replace(")", "");
            if (number.Length > 0 && number[0] != '+')
                number = "+" + number;
            return number;
        }

        public static bool IsValidAddress(this string address)
        {
            if (address == null)
                throw new ArgumentNullException(nameof(address), "Address cannot be null or empty or contain only white spaces");
            else if (string.IsNullOrWhiteSpace(address) || address.Length < 3 || !address.Any(symbol => char.IsLetter(symbol)) || !address.Any(symbol => char.IsDigit(symbol)))
                return false;
            else
                return true;
        }

        public static bool IsValidPassword(this string password)
        {
            if (password == null)
                throw new ArgumentNullException("Potential password can`t be null", nameof(password));
            const int minSize = 8;
            return password.Length >= minSize && !password.Any(symbol => char.IsWhiteSpace(symbol)) && password.Any(symbol => char.IsDigit(symbol)) && password.Any(symbol => char.IsUpper(symbol)) && password.Any(symbol => char.IsLower(symbol));
        }
    }
}
