﻿using System;
using System.Collections;
using System.Collections.Generic;
using ShopLogic.Interfaces;
using ShopLogic.HelperClasses;

namespace ShopLogic
{
    public class Order : IOrder
    {

        private static int _ordersQuantity = 0;

        /// <summary>
        /// Total quantity of orders
        /// </summary>
        /// <exception cref="InvalidOperationException">Thrown when customer quantity now is not 0 or given value is negative or equal 0 (in setter)</exception>
        public static int OrdersQuantity
        {
            get
            {
                return _ordersQuantity;
            }
            set
            {
                if (value >= _ordersQuantity && _ordersQuantity == 0)
                {
                    _ordersQuantity = value;
                }
                else
                {
                    throw new InvalidOperationException("CustomerQuantity may be setted only on creation of the project");
                }
            }
        }

        private CustomList<(IProduct product, int quantity)> _goods;

        public IBuyer Customer { get; private set; }

        public int ID { get; private set; }

        public OrderStatuses Status { get; set; }

        public DateTime Date { get; private set; }

        private string _address;

        public string Address
        {
            get => _address;
            private set
            {
                if (!value.IsValidAddress())
                    throw new FormatException("Wrong address format");
                _address = value;
            }
        }

        public bool IsConfirmed { get; private set; }

        public Order(IBuyer customer)
        {
            Customer = customer ?? throw new ArgumentNullException(nameof(customer), "Customer cannot be null");
            _goods = new CustomList<(IProduct product, int quantity)>();
            Status = OrderStatuses.New;
        }

        public Order(IEnumerable<(IProduct product, int quantity)> goods, IBuyer customer) : this(customer)
        {
            if (goods == null)
                throw new ArgumentNullException(nameof(goods), "IEnumerable of goods cannot be null");
            foreach (var item in goods)
                Add(item);
        }

        public void Confirm(string address, IDateTimeProvider timeProvider)
        {
            if (IsConfirmed)
                throw new InvalidOperationException("Order is already confirmed");
            if (_goods.Count < 1)
                throw new InvalidOperationException("You cannot confirm the empty order");
            Address = address;
            Date = timeProvider.Now;
            _ordersQuantity++;
            ID = _ordersQuantity;
            IsConfirmed = true;
        }

        public int Length => _goods.Count;

        public (IProduct product, int quantity) this[int index]
        {
            get
            {
                if (index < 0 || index >= _goods.Count)
                    throw new ArgumentOutOfRangeException(nameof(index), "Index cannot be negative or more than list of goods size");
                return (_goods[index].product.Clone() as IProduct, _goods[index].quantity);
            }
        }

        public void SetQuantityAt(int index, int quantity)
        {
            if (index < 0 || index >= _goods.Count)
                throw new ArgumentOutOfRangeException(nameof(index), "Index cannot be negative or more than list of goods size");
            else if (quantity <= 0)
                throw new ArgumentOutOfRangeException(nameof(quantity), "Quantity of any product cannot be negative or equal 0");
            else if (IsConfirmed)
                throw new InvalidOperationException("You cannot change the order beacuse it is already confirmed");
            else
                _goods[index] = (_goods[index].product, quantity);
        }

        public void DeleteAt(int index)
        {
            if (index < 0 || index >= _goods.Count)
                throw new ArgumentOutOfRangeException(nameof(index), "Index cannot be negative or more than list of goods size");
            if (IsConfirmed)
                throw new InvalidOperationException("You cannot change the order beacuse it is already confirmed");
            (IProduct product, int quantity) temp;
            for (int i = index; i < _goods.Count - 1; i++)
            {
                temp = _goods[i];
                _goods[i] = _goods[i + 1];
                _goods[i + 1] = temp;
            }
            CustomList<(IProduct product, int quantity)> newGoods = new CustomList<(IProduct product, int quantity)>();
            for (int i = 0; i < _goods.Count - 1; i++)
                newGoods.Add(_goods[i]);
            _goods = newGoods;
        }

        public void Clear()
        {
            if (IsConfirmed)
                throw new InvalidOperationException("You cannot change the order beacuse it is already confirmed");
            _goods = new CustomList<(IProduct product, int quantity)>();
        }

        public int Find(IProduct product)
        {
            for (int i = 0; i < _goods.Count; i++)
                if (_goods[i].product.Equals(product))
                    return i;
            return -1;
        }

        public void Add((IProduct product, int quantity) item)
        {
            if (item.product == null)
            {
                throw new ArgumentNullException(nameof(item.product), "Any item cannot be null");
            }
            else if (item.quantity <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(item.quantity), "Quantity of any product cannot be negative or equal 0");
            }
            else if (IsConfirmed)
            {
                throw new InvalidOperationException("You cannot change the order beacuse it is already confirmed");
            }
            else if (Find(item.product) != -1)
            {
                int index = Find(item.product);
                SetQuantityAt(index, _goods[index].quantity + item.quantity);
            }
            else
            {
                _goods.Add((item.product.Clone() as IProduct, item.quantity));
            }
        }

        public decimal CalculatePrice()
        {
            decimal totalPrice = 0;
            foreach (var (product, quantity) in _goods)
                totalPrice += product.Price * quantity;
            return totalPrice;
        }

        public IEnumerator<(IProduct product, int quantity)> GetEnumerator()
        {
            foreach (var (product, quantity) in _goods)
                yield return (product.Clone() as IProduct, quantity);
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
