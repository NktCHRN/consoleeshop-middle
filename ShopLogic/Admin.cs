﻿using ShopLogic.Interfaces;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ShopLogic
{
    public delegate void StatusHandler(object sender, StatusEventArgs e);

    public class Admin : RegisteredUser, IAdmin
    {
        public event StatusHandler StatusSetEvent;

        private readonly List<IOrder> _orders;

        public int OrdersQuantity => _orders.Count;

        private string _login = "Admin";

        /// <summary>
        /// "Admin" by default
        /// </summary>
        public string Login 
        { 
            get 
            {
                return _login;
            }
            set 
            {
                if (value == null)
                    throw new ArgumentNullException(nameof(value), "Login cannot be null or empty");
                if (value.Length < 2 || value.Any(symbol => char.IsWhiteSpace(symbol)))
                    throw new FormatException("Login should not contain white spaces and have length of at least 2 symbols");
                _login = value;
            } 
        }

        private const int _id = 0;
        public int ID => _id;

        public Admin(string password) : base(password)
        {
            _orders = new List<IOrder>();
        }

        public IOrder this[int index]
        {
            get
            {
                if (index < 0 || index >= _orders.Count)
                    throw new ArgumentOutOfRangeException(nameof(index), "Index cannot be negative or more than size of list of orders");
                return _orders[index];
            }
        }

        public override bool CheckLogin(string toCheck)
        {
            if (toCheck == null)
                throw new ArgumentNullException(nameof(toCheck), "Potential login cannot be null");
            return toCheck == Login;
        }

        public override string ToString() => Login;

        public void AddOrder(IOrder order)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order), "Order cannot be null");
            if (!order.IsConfirmed)
                throw new ArgumentException("Order to add to the history should be confirmed first", nameof(order));
            if (order.Customer.ID != ID)
                throw new ArgumentException("Order to add to the history should have the same customer ID", nameof(order));
            _orders.Add(order);
        }

        public bool CancelOrder(int index)
        {
            if (index < 0 || index >= _orders.Count)
            {
                throw new ArgumentOutOfRangeException(nameof(index), "Index cannot be negative or more than size of OrderHistory");
            }
            else if (_orders[index].Status >= OrderStatuses.Received)
            {
                return false;
            }
            else
            {
                _orders[index].Status = OrderStatuses.CancelledByUser;
                return true;
            }
        }

        public void SetStatus(IOrderDetails order, OrderStatuses newStatus)
        {
            if (order == null)
                throw new ArgumentNullException(nameof(order), "Order cannot be null");
            if (newStatus == OrderStatuses.CancelledByUser)
                throw new ArgumentException("Admin cannot set status \"CancelledByUser\"", nameof(newStatus));
            OrderStatuses oldStatus = order.Status;
            order.Status = newStatus;
            StatusSetEvent?.Invoke(this, new StatusEventArgs(order, oldStatus, order.Status));
        }

        public IEnumerator<IOrder> GetEnumerator() => _orders.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}
