﻿using System.Collections.Generic;

namespace ShopLogic.Interfaces
{
    public interface IGettableProductCollection : IEnumerable<(IProduct product, int quantity)>
    {

        public int Length { get; }

        public (IProduct product, int quantity) this[int index] { get; }

        public int Find(IProduct product);
    }
}
