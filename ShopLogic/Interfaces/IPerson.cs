﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ShopLogic.Interfaces
{
    public interface IPerson
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public string GetFullName() => $"{FirstName} {LastName}";
    }
}
