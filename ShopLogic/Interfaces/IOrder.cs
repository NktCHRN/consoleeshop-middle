﻿namespace ShopLogic.Interfaces
{
    public interface IOrder : IOrderProductList, IOrderDetails { }
}
