﻿namespace ShopLogic.Interfaces
{
    public interface INotifier
    {
        public event StatusHandler StatusSetEvent;
    }
}
