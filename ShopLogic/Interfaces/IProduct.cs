﻿using System;

namespace ShopLogic.Interfaces
{
    public interface IProduct : ICloneable
    {
        public string Name { get; set; }

        public Categories Category { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }
    }
}
