﻿namespace ShopLogic.Interfaces
{
    public interface IAdmin : INotifier, IBuyer, IAuthorizable, INonCustomerStatusSetter
    {
        public string Login { get; set; }
    }
}
