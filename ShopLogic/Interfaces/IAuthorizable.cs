﻿namespace ShopLogic.Interfaces
{
    public interface IAuthorizable
    {

        public string Password { set; }

        public bool CheckLogin(string toCheck);

        public bool CheckPassword(string toCheck);
    }
}
