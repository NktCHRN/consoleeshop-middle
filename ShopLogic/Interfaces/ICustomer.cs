﻿namespace ShopLogic.Interfaces
{
    public interface ICustomer : IAuthorizable, IBuyer, INotifiable, IPerson { }
}
