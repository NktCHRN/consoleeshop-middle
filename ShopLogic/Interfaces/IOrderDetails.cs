﻿using System;

namespace ShopLogic.Interfaces
{
    public interface IOrderDetails
    {
        public IBuyer Customer { get; }

        public int ID { get; }

        public OrderStatuses Status { get; set; }

        public DateTime Date { get; }

        public string Address { get; }

        public decimal CalculatePrice();
    }
}
