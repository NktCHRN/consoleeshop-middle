﻿using System;

namespace ShopLogic.Interfaces
{
    public interface IDateTimeProvider
    {
        DateTime Now { get; }
    }
}
