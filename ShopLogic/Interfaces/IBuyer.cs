﻿using System.Collections.Generic;

namespace ShopLogic.Interfaces
{
    public interface IBuyer : IEnumerable<IOrder>
    {

        public int ID { get; }

        public IOrder this[int index] { get; }

        public int OrdersQuantity { get; }

        public void AddOrder(IOrder order);
    }
}
